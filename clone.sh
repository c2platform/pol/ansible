#!/bin/bash
export REPO_DIR=~/git/gitlab/pol
export BRNCH=master
export C2_CLONE_METHOD="${C2_CLONE_METHOD:-ssh}"

if [ -n "$GIT_USER" ] && [ -n "$GIT_MAIL" ]; then
  echo "GIT_USER: $GIT_USER"
  echo "GIT_MAIL: $GIT_MAIL"
else
  echo "You should set GIT_USER and GIT_MAIL"
  exit 1
fi
if [ ! -d "$REPO_DIR" ] # Create REPO_DIR if not exist
then
  echo "Create REPO_DIR: $REPO_DIR"
  mkdir -p $REPO_DIR
fi
clone_repo () {
  cd $REPO_DIR
  if [ ! -d "$2" ]
  then
    git clone $1 $2
    cd $2
    git fetch origin $3
    git checkout $3
    git branch --set-upstream-to=origin/$3 $3
    git config pull.rebase false
    git pull origin $3
    git config user.name "$GIT_USER"
    git config user.email "$GIT_MAIL"
  else  # pull changes
    cd $2
    git pull origin $3
  fi
  cd $REPO_DIR
  if [ "$C2_LINTERS" == "Y" ]
  then
    cd $2
    if [ -f ".ansible-lint" ]
    then
      echo "Creating / updating $2/.git/hooks/pre-commit"
      curl -s -L https://gitlab.com/c2platform/ansible/-/raw/master/doc/howto-development/pre-commit > .git/hooks/pre-commit
      chmod 775 .git/hooks/pre-commit
    fi
  fi
  cd $REPO_DIR
  if [ "$C2_LINTERS" == "N" ]
  then
    cd $2
    if [ -f ".ansible-lint" ]
    then
      echo "Removing $2/.git/hooks/pre-commit"
      rm .git/hooks/pre-commit
    fi
  fi
}
if [ "$C2_CLONE_METHOD" == "ssh" ]
then
  clone_repo "git@gitlab.com:c2platform/pol/ansible.git" "ansible-dev" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-apps.git" "ansible-dev-collections/ansible_collections/c2platform/apps" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-core.git" "ansible-dev-collections/ansible_collections/c2platform/core" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-mgmt.git" "ansible-dev-collections/ansible_collections/c2platform/mgmt" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-mw.git" "ansible-dev-collections/ansible_collections/c2platform/mw" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-test.git" "ansible-dev-collections/ansible_collections/c2platform/test" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-oracle.git" "ansible-dev-collections/ansible_collections/c2platform/oracle" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-dev.git" "ansible-dev-collections/ansible_collections/c2platform/dev" master
fi
if [ "$C2_CLONE_METHOD" == "https" ]
then
  echo "[WARNING] Not implemented yet!"
fi